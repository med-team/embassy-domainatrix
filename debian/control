Source: embassy-domainatrix
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               architecture-is-64-bit,
               emboss-lib,
               libx11-dev,
               libgd-dev,
               default-libmysqlclient-dev,
               libpq-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/embassy-domainatrix
Vcs-Git: https://salsa.debian.org/med-team/embassy-domainatrix.git
Homepage: https://emboss.sourceforge.net/apps/cvs/embassy/index.html#DOMAINATRIX
Rules-Requires-Root: no

Package: embassy-domainatrix
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         emboss-lib,
         emboss-lib (<< 6.6.1~)
Recommends: emboss
Suggests: embassy
Description: Extra EMBOSS commands to handle domain classification file
 The DOMAINATRIX programs were developed by Jon Ison and colleagues at MRC HGMP
 for their protein domain research. They are included as an EMBASSY package as
 a work in progress.
 .
 Applications in the current domainatrix release are cathparse (generates DCF
 file from raw CATH files), domainnr (removes redundant domains from a DCF
 file), domainreso (removes low resolution domains from a DCF file), domainseqs
 (adds sequence records to a DCF file), domainsse (adds secondary structure
 records to a DCF file), scopparse (generates DCF file from raw SCOP files) and
 ssematch (searches a DCF file for secondary structure matches).
